module.exports = (grunt) ->
  require('load-grunt-tasks')(grunt)
  require('time-grunt')(grunt)

  appConfig =
    app: require('./bower.json').appPath || 'app',
    dist: 'dist'

  # Project configuration.
  grunt.initConfig

    config: appConfig

    bower_concat:
      dist:
        dest: '<%= config.app %>/js/vendor.js'

    concat:
      dist:
        src: [
          '<%= config.app %>/js/app.js',
          '<%= config.app %>/js/config.js',
          '<%= config.app %>/js/router.js',
          '<%= config.app %>/js/directives/directives.js',
          '<%= config.app %>/js/directives/**/*.js',
          '<%= config.app %>/js/controllers/controllers.js',
          '<%= config.app %>/js/controllers/**/*.js',
          '<%= config.app %>/js/services/services.js',
          '<%= config.app %>/js/services/**/*.js',
          '<%= config.app %>/js/models/models.js',
          '<%= config.app %>/js/models/**/*.js'
        ]
        dest: '<%= config.app %>/tmp/app.js'


    cssmin:
      dist:
        files: [
          expand: true,
          cwd: '<%= config.app %>/css',
          src: ['*.css'],
          dest: '<%= config.dist %>/css',
          ext: '.min.css'
        ]

    uglify:
      dist:
        options:
          mangle: false,
          compress: true
        files: [
          '<%= config.dist %>/js/vendor.min.js': '<%= config.app %>/js/vendor.js',
          '<%= config.dist %>/js/app.min.js': '<%= config.app %>/tmp/app.js',
        ]

    copy:
      dist:
        files: [
          {
            expand: true
            dot: true
            cwd: '<%= config.app %>'
            dest: '<%= config.dist %>'
            src: [
              '*.html',
              'views/**/*.html'
            ]
          }
        ]

  grunt.registerTask 'build', ['bower_concat', 'concat', 'uglify', 'cssmin', 'copy']