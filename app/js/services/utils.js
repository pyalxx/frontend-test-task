angular.module('task.services').service('Utils',function(){
    return {
        parseData: function(klass, data){
            var result = [];
            angular.forEach(data, function (item) {
                result.push(new klass(item));
            });
            return result;
        }
    }
});