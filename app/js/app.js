angular.module('task',
        [
            'ui.router',
            'restangular',
            'task.configuration',
            'task.router',
            'task.services',
            'task.models',
            'task.controllers',
            'task.directives'
        ])
    .config(function (RestangularProvider, host) {
        RestangularProvider.setBaseUrl(host+'/admin');
    });
