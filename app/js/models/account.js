angular.module('task.models').factory('Account', function (Restangular, Utils, Transaction) {

    var service = Restangular.service('accounts');

    function Account(account) {
        this.data = account;
    }

    Account.prototype = {
        getTransactions: function () {
            var that = this;
            return service.one(this.data.id).all('transactions').getList().then(function (transactions) {
                that.data.transactions = Utils.parseData(Transaction, transactions);
            });
        },
        select: function () {
            this.selected = !this.selected;
            if (!this.data.transactions) {
                this.getTransactions();
            }
        }
    };

    Account.getAll = function () {
        return service.getList().then(function (accounts) {
            return Utils.parseData(Account, accounts);
        });
    };
    return (Account);
});