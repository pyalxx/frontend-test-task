angular.module('task.models').factory('Transaction', function (Restangular, Utils) {

    var service = Restangular.service('transactions');

    function Transaction(transaction) {
        this.data = transaction;
    }

    Transaction.prototype = {
        updateStatus: function (status) {
            var that = this;
            return service.one(this.data.id).customPUT({transaction: {status: status}}).then(function (transaction) {
                that.data = transaction;
                that.data.currentStatus = transaction.status;
                return transaction;
            });
        },
        statusChanged: function () {
            return !(this.data.currentStatus == this.data.status);
        },
        tableClass: function () {
            if (this.statusChanged()) {
                return 'active';
            } else {
                switch (this.data.status) {
                    case 'succeeded':
                        return 'success';
                        break;
                    case 'pending':
                        return 'warning';
                        break;
                    case 'failed':
                        return 'danger';
                        break;
                    default:
                        return ''
                }
            }
        }
    };

    Transaction.getAll = function () {
        return service.getList().then(function (transactions) {
            var result = [];
            angular.forEach(Utils.parseData(Transaction, transactions), function (item) {
                item.data.currentStatus = item.data.status;
                result.push(item);
            });
            return result;
        });
    };

    return (Transaction);
});