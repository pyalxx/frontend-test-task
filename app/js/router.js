angular.module('task.router', []).config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/accounts');

    $stateProvider
        .state('accounts', {
            url: '/accounts',
            templateUrl: 'views/accounts.html',
            controller: 'accountsCtrl'
        })
        .state('transactions', {
            url: '/transactions',
            templateUrl: 'views/transactions.html',
            controller: 'transactionsCtrl'
        });
})