angular.module('task.controllers').controller('accountsCtrl', function ($scope, Account) {
    $scope.accounts = [];

    Account.getAll().then(function (data) {
        $scope.accounts = data;
    });
});