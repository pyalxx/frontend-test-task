angular.module('task.controllers').controller('transactionsCtrl',function($scope, Transaction){
    $scope.transactions = [];
    $scope.statuses = ['succeeded', 'pending', 'failed'];

    Transaction.getAll().then(function (data) {
        $scope.transactions = data;
    });
 });