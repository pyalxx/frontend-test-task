angular.module('task.directives').directive('tabs', function () {
    return {
        restrict: 'E',
        templateUrl: 'views/partials/tabs.html',
        controller: function ($scope, $state) {
            $scope.state = $state.current.name;
        }
    };
});
